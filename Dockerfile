# Pull base image.
FROM ubuntu:14.04
MAINTAINER Taeho Kim <taeho@coupang.com>

# Install basic packages.
RUN \
  sed -i 's/# \(.*multiverse$\)/\1/g' /etc/apt/sources.list && \
  apt-get update && \
  apt-get -y upgrade && \
  apt-get install -y build-essential && \
  apt-get install -y software-properties-common && \
  apt-get install -y byobu curl git htop man unzip vim wget

# Install Python packages.
RUN \
  apt-get install -y python python-dev python-pip python-software-properties

# Install Oracle JDK 7.
# Changed to 8 - Luke
RUN \
  add-apt-repository ppa:webupd8team/java && \
  apt-get update && \
  echo oracle-java7-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections && \
  apt-get -y install oracle-java8-installer

# Clean apt-get lists.
RUN rm -rf /var/lib/apt/lists/*

# Set environment variables.
ENV HOME /root

# Define working directory.
WORKDIR /root

# Change TimeZone to US/Pacific from UTC.
RUN \
  rm /etc/localtime && \
  ln -s /usr/share/zoneinfo/US/Pacific /etc/localtime

# Define working directory.
WORKDIR /data

# Define commonly used JAVA_HOME variable
ENV JAVA_HOME /usr/lib/jvm/java-8-oracle

# Install presto-cli
ADD https://repo1.maven.org/maven2/com/facebook/presto/presto-cli/0.114/presto-cli-0.114-executable.jar /usr/local/bin/presto
RUN chmod +x /usr/local/bin/presto

ENTRYPOINT ["/usr/local/bin/presto"]
